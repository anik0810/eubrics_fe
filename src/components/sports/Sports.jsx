/* import React, { useEffect, useState } from 'react';
import './Sports.css'
import Item from '../Item/Item';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusSquare } from '@fortawesome/free-regular-svg-icons';
import SportsModal from './SportsModal';

import { useDispatch, useSelector } from 'react-redux';
import { getSportsData } from '../../state/slice/SportsSlice';
import SportsItem from './SportsItem';

export default function Sports() {

    const [list, setList] = useState([]);
    const [addModal, setAddModal] = useState(false);

    const dispatch = useDispatch();
    const state = useSelector((state) => state)
    console.log(state.Sports.data)


    useEffect(async () => {
        document.title = 'Sports - ToDo App';
        const userDetails = JSON.parse(await localStorage.getItem('userDetails'));

        if (state.Sports.data === null) {
            dispatch(getSportsData(userDetails.userId));
            setList(state.Sports.data);
        }

    }, [])



    return (
        <>
            <div className='sports'>
                <h2>Sports</h2>
                <div className='container mt-4'>
                {(state.Sports.isLoading)?<>Loading</>:
                    (state.Sports.data) ?
                        state.Sports.data.map((key, val) => {
                            return (
                                <>
                                    <SportsItem data={key} />
                                </>
                            )
                        }) : null
                    }
                </div>
            </div>
            <div className='add'>
                <button className='btn btn-primary' onClick={() => { setAddModal(true) }}>
                    <span>
                        <FontAwesomeIcon icon={faPlusSquare} />
                    </span>
                </button>

            </div>
            <SportsModal
                show={addModal}
                onHide={() => setAddModal(false)}
                heading="Add"
            />

        </>
    )
}
 */

import React from 'react'

export default function Sports() {
  return (
    <div>Sports</div>
  )
}
