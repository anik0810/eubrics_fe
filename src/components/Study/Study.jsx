import React, { useEffect, useState } from 'react';
import './Study.css'
import Item from '../Item/Item';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusSquare } from '@fortawesome/free-regular-svg-icons';
import StudyModal from './StudyModal';

import { useDispatch, useSelector } from 'react-redux';
import { getStudyData } from '../../state/slice/studySlice';
import StudyItem from './StudyItem';

export default function Study() {

    const [list, setList] = useState([]);
    const [addModal, setAddModal] = useState(false);

    const dispatch = useDispatch();
    const state = useSelector((state) => state)
    console.log(state.study.data)


    useEffect(async () => {
        document.title = 'Study - ToDo App';
        const userDetails = JSON.parse(await localStorage.getItem('userDetails'));

        if (state.study.data === null) {
            dispatch(getStudyData(userDetails.userId));
            setList(state.Study.data);
        }

    }, [])



    return (
        <>
            <div className='study'>
                <h2>Study</h2>
                <div className='container mt-4'>
                {(state.study.isLoading)?<>Loading</>:
                    (state.study.data) ?
                        state.study.data.map((key, val) => {
                            return (
                                <>
                                    <StudyItem data={key} />
                                </>
                            )
                        }) : null
                    }
                </div>
            </div>
            <div className='add'>
                <button className='btn btn-primary' onClick={() => { setAddModal(true) }}>
                    <span>
                        <FontAwesomeIcon icon={faPlusSquare} />
                    </span>
                </button>

            </div>
            <StudyModal
                show={addModal}
                onHide={() => setAddModal(false)}
                heading="Add"
            />

        </>
    )
}
