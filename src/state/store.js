import { configureStore} from "@reduxjs/toolkit";
import userAuthReducer from "./slice/userAuth";
import getHealthSlice from './slice/healthSlice'
import getStudySlice from "./slice/studySlice";

export const store = configureStore({
    reducer:{
        auth:userAuthReducer,
        health:getHealthSlice,
        study:getStudySlice
    },
});