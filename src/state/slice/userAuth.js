import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { AuthApi } from "../../apis/authApi";
import { useNavigate } from "react-router-dom";

export const signInUser = createAsyncThunk('signInUser', async (userCredential) => {
    const response = await AuthApi.signIn(userCredential);
    console.log(response.data);
    return response.data;
})

export const signUpUser = createAsyncThunk('signUpUser', async (userDetails) => {
    const response = await AuthApi.signUp(userDetails);
    console.log(response.data);
    return response.data;
})

const authSlice = createSlice({
    name: 'login',
    initialState: {
        isLoading: false,
        data: null,
        hasError: null
    },
    extraReducers: (builder) => {
        builder.addCase(signInUser.pending, (state, action) => {
            state.isLoading = true;
        });
        builder.addCase(signInUser.rejected, (state, action) => {
            state.hasError = true;
            console.log('error', action);
        });
        builder.addCase(signInUser.fulfilled, (state, action) => {
            state.isLoading = false;
            state.data = action.payload;
            state.hasError = false;
            localStorage.setItem('isLoggedIn', true);
            localStorage.setItem('userDetails', JSON.stringify(action.payload))

        });

        builder.addCase(signUpUser.pending, (state, action) => {
            state.isLoading = true;
        });
        builder.addCase(signUpUser.rejected, (state, action) => {
            state.hasError = true;
            console.log('error', action);
        });
        builder.addCase(signUpUser.fulfilled, (state, action) => {
            state.isLoading = false;
            state.data = action.payload;
            state.hasError = false;
            localStorage.setItem('isLoggedIn', true);
            localStorage.setItem('userDetails', JSON.stringify(action.payload))

        });

    }
})

export default authSlice.reducer;