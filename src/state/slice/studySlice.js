import { createSlice,createAsyncThunk } from "@reduxjs/toolkit";
import { StudyApi } from "../../apis/studyApi";

export const addStudyData = createAsyncThunk('addStudyData',async (StudyDetails)=>{
    const response = await StudyApi.postStudy(StudyDetails);
    console.log(response);
    return response;
})
export const getStudyData = createAsyncThunk('getStudyData',async (userId)=>{
    const response = await StudyApi.getAllStudy(userId);
    console.log(response);
    return response;
})
export const updateStudyData = createAsyncThunk('updateStudyData',async (studyDetails)=>{

    const response = await StudyApi.updateStudy(studyDetails.payload,studyDetails.id);
    console.log(response);
    return response;
})
export const deleteStudyData = createAsyncThunk('deleteStudyData',async (deleteData)=>{

    const response = await StudyApi.deleteStudy(deleteData.id,deleteData.userId);
    console.log(response);
    return response;
})

const StudySlice = createSlice({
    name: 'getStudy',
    initialState:{
        isLoading:false,
        data:null,
        hasError:false
    },
    extraReducers: (builder)=>{
        builder.addCase(getStudyData.pending,(state,action)=>{
            state.isLoading=true;
        });
        builder.addCase(getStudyData.fulfilled,(state,action)=>{
            state.isLoading=false;
            state.data=action.payload;
            state.hasError=false;
        });
        builder.addCase(getStudyData.rejected,(state,action)=>{
            state.hasError=true;
            console.log('error',action);
        });
        builder.addCase(addStudyData.pending,(state,action)=>{
            state.isLoading=true;
        });
        builder.addCase(addStudyData.fulfilled,(state,action)=>{
            state.isLoading=false;
            state.data.push(action.payload);
            state.hasError=false;
        });
        builder.addCase(addStudyData.rejected,(state,action)=>{
            state.hasError=true;
            console.log('error',action);
        });
        builder.addCase(updateStudyData.pending,(state,action)=>{
            state.isLoading=true;
        });
        builder.addCase(updateStudyData.fulfilled,(state,action)=>{
            state.isLoading=false;
            state.data=action.payload;
            state.hasError=false;
        });
        builder.addCase(updateStudyData.rejected,(state,action)=>{
            state.hasError=true;
            console.log('error',action);
        });
        builder.addCase(deleteStudyData.pending,(state,action)=>{
            state.isLoading=true;
        });
        builder.addCase(deleteStudyData.fulfilled,(state,action)=>{
            state.isLoading=false;
            state.data=action.payload;
            state.hasError=false;
        });
        builder.addCase(deleteStudyData.rejected,(state,action)=>{
            state.hasError=true;
            console.log('error',action);
        });

    },
})

export default StudySlice.reducer;