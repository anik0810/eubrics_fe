import { api } from "./configs/axiosConfig";

export const StudyApi={
    getAllStudy : async function(userId){
        const response = await api.request({
            url:`study/getStudy/${userId}`,
            method:"GET"
        })
        return response.data;
    },

    postStudy : async function(postStudyTodo){
        const response= await api.request({
            url:'study/addStudy',
            method:'POST',
            data:postStudyTodo
        })
        return response.data;
    },

    updateStudy:async function(putStudyToDo,id){
        const response= await api.request({
            url:`study/updateStudy/${id}`,
            method:'PATCH',
            data:putStudyToDo
        })
        return response.data;
    },

    deleteStudy:async function(id,userId){
        const response= await api.request({
            url:`study/deleteHealth/${id}/${userId}`,
            method:'DELETE',
        })
        return response.data;
    }
    
}